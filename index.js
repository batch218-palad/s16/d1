// console.log("hello world!");

// [SECTION] Assignment Operator
	//Badic assg]ignment operator (=)
	// assigns the value of the right hand operand to a variable
	let assignmentNumber = 8;
	console.log("Thes value of the assignmentNumber variable: " + assignmentNumber)
	console.log("------------")

// [SECTION] Arithmetic Operations

	let x = 200;
	let y = 18;

	console.log("x: " + x)
	console.log("y: " + y)
	console.log("------------")

	// Addition
	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// Subtraction
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	// Multiplication
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// Division
	let qoutient = x / y;
	console.log("Result of division operator: " + qoutient);

	let modulo = x % y;
	console.log("Result of modulo operator: " + modulo);

//  Continuation of assignment operator

// Addition assignment operator

	// current value of assignmentNumber is 8
	// assignmentNumber = assignmentNumber + 2; // long method
	// console.log(assignmentNumber)

	// short method
	assignmentNumber += 2;
	console.log("Result of Addition assigment operator: " + assignmentNumber); //assignmentNumber value is now 10

// Subtraction assignment op"Result of " + erator
	// assignmentNumber = assignmentNumber - 3;
	// console.log(assignmentNumber); //7

	
	assignmentNumber -= 3;
	console.log("Result of Subtraction assigment operator: " + assignmentNumber);


// Multiplication assignment operator
	assignmentNumber *= 2;
	console.log("Result of Multiplication assigment operator: " + assignmentNumber);

// Multiplication assignment operator
	assignmentNumber /= 2;
	console.log("Result of Division assigment operator: " + assignmentNumber);


// [SECTION] PEMDAS (Order of Operations)
// Multiple Operatos and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);
	/*
		The operations were done in the following order:
		1. 3*4 = 12  |  1 + 2 - 12/5
		2. 12/5=2.4  |  1+2 -2.4
		3. 
		4.
	*/

let pemdas = (1+(2-3)) * (4/5);
console.log("Result of pemdas operation: " + pemdas)

// Heirarchy
// combinations of multiple arithmetic operators will follow the pemdas rule parenthesis, exponent, M or D, + or -

	// NOTE: l to r rule







// [SECTION] Increment and Decrement
// Increment - pagdagdag
// Decrement - pagbawas

// Operators that add or subtract values by 1 and reassign the value of the variable where the increment(++) or decrement(--) was applied.

let z = 1;

// pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment); // 2
console.log("Result of pre-increment of z: " + z); // 2

// post-increment
increment = z++;
console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment of z: " + z); //3
// increment = z++;
// console.log("Result of post-increment: " + increment);
// console.log("Result of post-increment of z: " + z);


let decrement = --z;
console.log("Result of pre-decrement: " + decrement); //2
console.log("Result of pre-decrement of z: " + z); //2

decrement = z--;
console.log("Result of post-decrement: " + decrement); //2
console.log("Result of post-decrement of z: " + z); //1



// [SECTION] Type Coercion
	// automatic convertion of values from one data type to another

	// combination if number and string data type will result to string (if string is involved, result is always string)
	let numA = 10;   //number
	let numB = "12"; //String

	let coercion = numA + numB;
	console.log(coercion); //1012
	console.log(typeof coercion);

	//  number + number = number
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion); //30
	console.log(typeof nonCoercion);

	// combination of number and boolean data type will result to number
	let numX = 10;
	let numY = true;
			// boolean values: true=1, false=0
	coercion = numX + numY;
	console.log(coercion);
	console.log(typeof coercion);

	let num1 = 1;
	let num2 = false;
	coercion = num1 + num2;
	console.log(coercion);
	console.log(typeof coercion);



// [SECTION] Comparison Operators
		// used to evaluate and compare left and right operands
	// After evaluation, it returns a boolean value

// equality operator (== read as equal to)
	// compares value but not data type
console.log( 1 == 1); //true
console.log( 1 == 2); //false
console.log( 1 == '1') //true
console.log( 0 == false); //true

let juan = "juan";
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); //false //equality operator is strict will letter casing
console.log(juan == 'juan'); //true

console.log("-------")

//inequality operator (!=  also read as not equal)
console.log( 1 != 1); //false
console.log( 1 != 2); //true
console.log( 1 != '1') //false
console.log( 0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != 'Juan'); //true
console.log(juan != 'juan'); //false

// [SECTION] Relational Operators
//  SOme comparison operators to check weather a value is  greater or less than another value
// Will return a value of true or false

console.log("-------")

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b; //false
console.log(isGreaterThan);

// LT or Less Than Operator (<)
let isLessThan = a < b; //true
console.log(isLessThan);

// GTE or Greater Than or Equal Operator (>=)  --only needs to satisfy one true value to result a true value
let isGTorEqual = a >= b; //false
console.log(isGTorEqual);

// LTE or Less Than or Equal Operator (>=)  --only needs to satisfy one true value to result a true value
let isLTorEqual = a <= b; //true
console.log(isLTorEqual);

// Forced Coercion
let num = 56;
let numStr = "30";
console.log(num > numStr); // true
// Since string is numeric, the string will not be converted into a number, or a.k.a NaN (Not a Number)

let str = "twenty";
console.log(num > str);

// Locical AND operator (&&)
console.log("--------")
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);
// AND operator requires all / both are true

// Logical OR operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);
// OR operators requires only one true

// Logical Not Operator (!)
	//returns the oposite value
let someRequirementsNotMet = !isLegalAge;
console.log("Result of logical NOT operator: " + someRequirementsNotMet)







